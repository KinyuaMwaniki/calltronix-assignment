# Calltronix assignment

Social

## System Requirements

php 8.1
node 16

u: janedoe@mailinator.com
p: password

## Deployment

To deploy this project run

```bash
git clone https://gitlab.com/KinyuaMwaniki/calltronix-assignment.git
cd calltronix-assignment
composer install
npm install
npm run build
cp .env.example .env
php artisan key:generate
Create db and enter database name, user, and password in .env
php artisan migrate --seed
php artisan storage:link
chmod 777 -R storage
set up your local server
Add sanctum credentials to the .env as below (depends on your local url and port)
SESSION_DOMAIN=calltronix-assignment.bar
SANCTUM_STATEFUL_DOMAINS=calltronix-assignment.bar:8080
