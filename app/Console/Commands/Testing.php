<?php

namespace App\Console\Commands;

use App\Models\Book;
use App\Models\User;
use App\Models\Product;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Storage;

class Testing extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'testing';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'tests code';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        Artisan::call('log:clear');

        DB::enableQueryLog();
        // $query = DB::table('users')->toSql();
        // User::all();
        $products = Product::select('name')->withSum('orders', 'quantity')->orderBy('orders_sum_quantity', 'desc')->get();

        foreach ($products as $product) {
            info('$product');
            info($product);
        }
        // info($query);
        info(DB::getQueryLog());
    }
}
