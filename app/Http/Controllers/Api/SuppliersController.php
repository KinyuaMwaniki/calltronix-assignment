<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use App\Models\Supplier;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Http\Resources\UserCollection;
use Illuminate\Support\Facades\Artisan;
use App\Http\Resources\SupplierResource;
use App\Http\Resources\SupplierCollection;
use App\Http\Requests\suppliers\UpdateRequest;

class SuppliersController extends Controller
{
    public function index(Request $request)
    {
        $query = Supplier::query();

        if ($request->filled('search')) {
            $search = $request->search;
            $query->where(function ($query) use ($search) {
                $query->where('name', 'like', '%' . $search . '%');
            });
        }

        if ($request->filled('perPage')) {
            $per_page = $request->perPage;
            $users = $query->paginate((int) $per_page);
            return  new SupplierCollection($users);
        }

        return  SupplierResource::collection($query->get());
    }

    public function update(UpdateRequest $request, Supplier $supplier)
    {
        $supplier->update($request->validated());
        return response()->json(null, 200);
    }

    public function destroy(Supplier $supplier)
    {
        $supplier->delete();
        return response()->json(null, 204);
    }
}
