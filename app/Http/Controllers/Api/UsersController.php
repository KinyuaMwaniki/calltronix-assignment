<?php

namespace App\Http\Controllers\Api;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Http\Resources\UserCollection;
use Illuminate\Support\Facades\Artisan;
use App\Http\Requests\users\UpdateRequest;

class UsersController extends Controller
{
    public function index(Request $request)
    {
        $query = User::query();

        if ($request->filled('search')) {
            $search = $request->search;
            $query->where(function ($query) use ($search) {
                $query->where('name', 'like', '%' . $search . '%');
            });
        }

        // if ($request->filled('staff')) {
        //     $query->where('type', 'Staff');
        // }

        if ($request->filled('perPage')) {
            $per_page = $request->perPage;
            $users = $query->paginate((int) $per_page);
            return  new UserCollection($users);
        }

        return  UserResource::collection($query->get());
    }

    public function update(UpdateRequest $request, User $user)
    {
        $user->update(array_merge(
            $request->validated(),
            [
                'active' => (int) $request->active
            ]
        ));
        return response()->json(null, 200);
    }

    public function destroy(User $user)
    {
        $user->delete();
        return response()->json(null, 204);
    }
}
