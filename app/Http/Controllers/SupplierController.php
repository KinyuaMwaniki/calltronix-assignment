<?php

namespace App\Http\Controllers;

use App\Models\Supplier;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\suppliers\CreateRequest;
use App\Http\Requests\suppliers\UpdateRequest;

class SupplierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('suppliers.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('suppliers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request)
    {
        Supplier::create(
            $request->validated()
        );
        Session::flash('message', "Supplier Saved");
        return redirect(route('suppliers.index'));
    }

    public function show(Supplier $supplier)
    {
        //
    }

    public function edit(Supplier $supplier)
    {
        if (empty($supplier)) {
            return redirect(route('suppliers.index'));
        }

        return view('suppliers.edit', compact(['supplier']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, Supplier $supplier)
    {
        if (empty($supplier)) {
            Session::flash('message', "Supplier Not Found");
            return redirect(route('suppliers.index'));
        }
        $supplier->update(
            $request->validated(),
        );
        Session::flash('message', "Supplier Updated");
        return redirect(route('suppliers.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }
}
