<?php

namespace App\Http\Requests\profile;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string', 'max:255'],
            'size' => ['nullable', 'integer', 'max:255'],
            'country_code' => ['nullable', 'string', 'max:255'],
            'whatsapp_country_code' => ['nullable', 'string', 'max:255'],
            'company_description' => ['nullable', 'string', 'max:255'],
            'industry' => ['required', 'string', 'max:255'],
            'start_date' => ['nullable', 'string', 'max:255'],
            'phone' => ['nullable', 'string', 'max:255'],
            'whatsapp_phone' => ['nullable', 'string', 'max:255'],
            'logo' => [
                'nullable', 'image', 'max:3000', 'mimes:jpg,png,jpeg,gif,svg'
            ],
            'contact_person_name' => ['nullable', 'string', 'max:255'],
            'contact_person_country_code' => ['nullable', 'string', 'max:255'],
            'contact_person_designation' => ['nullable', 'string', 'max:255'],
            'contact_person_email' => ['nullable', 'email', 'string', 'max:255'],
            'contact_person_phone' => ['nullable', 'string', 'max:255'],
        ];
    }
}
