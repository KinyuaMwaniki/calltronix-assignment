<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class Company extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'size',
        'country_code',
        'whatsapp_country_code',
        'company_description',
        'industry',
        'start_date',
        'phone',
        'whatsapp_phone',
        'contact_person_name',
        'contact_person_country_code',
        'contact_person_designation',
        'contact_person_email',
        'contact_person_phone',
        'addresses'
    ];
}
