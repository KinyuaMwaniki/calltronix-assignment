<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Jenssegers\Mongodb\Eloquent\Model;

class File extends Model
{
    use HasFactory;

    public $fillable = [
        'reference_type',
        'reference_id',
        'original_name',
        'name',
        'file_ext',
        'file_path'
    ];
}
