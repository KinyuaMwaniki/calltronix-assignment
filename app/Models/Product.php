<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'description',
        'model',
    ];


    public function orders()
    {
        return $this->hasMany(OrderProduct::class, 'product_id');
    }
}
