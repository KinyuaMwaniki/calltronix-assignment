<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PurchaseOrder extends Model
{
    use HasFactory;

    protected $fillable = [
        'order_number',
        'supplier_id'
    ];

    public function products()
    {
        return $this->hasMany(PurchaseOrderProduct::class, 'order_id');
    }
}
