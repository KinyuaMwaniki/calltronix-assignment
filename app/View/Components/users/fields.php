<?php

namespace App\View\Components\users;

use App\Models\Supplier;
use Illuminate\View\Component;

class fields extends Component
{
    public $supplier;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(Supplier $supplier = null)
    {
        $this->supplier = $supplier;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.suppliers.fields');
    }
}
