<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use App\Models\Master;
use App\Models\Company;
use App\Models\Merchant;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Hash;

class MerchantsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $merchantOne = User::where('email', 'merchantone@mailinator.com')->first();
        $password = 'password';

        if (!$merchantOne) {
            $merchantOne = User::create([
                'name' => 'Merchant One',
                'email' => 'merchantone@mailinator.com',
                'password' => Hash::make($password),
                'email_verified_at' => now(),
                'role' => 'Merchant'
            ]);
        }

        $merchantTwo = User::where('email', 'merchanttwo@mailinator.com')->first();
        $password = 'password';

        if (!$merchantTwo) {
            $merchantTwo = User::create([
                'name' => 'Merchant Two',
                'email' => 'merchanttwo@mailinator.com',
                'password' => Hash::make($password),
                'email_verified_at' => now(),
                'role' => 'Merchant'
            ]);
        }
    }
}
