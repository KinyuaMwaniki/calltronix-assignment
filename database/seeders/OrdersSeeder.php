<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\Order;
use App\Models\Product;
use App\Models\Supplier;
use App\Models\OrderProduct;
use Illuminate\Database\Seeder;

class OrdersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cookingOil = Product::where('name', 'Cooking Oil')->first();
        $shoePolish = Product::where('name', 'Shoe Polish')->first();
        $toothpaste = Product::where('name', 'Toothpaste')->first();

        $merchantOne = User::where('email', 'merchantone@mailinator.com')->first();
        $merchantTwo = User::where('email', 'merchanttwo@mailinator.com')->first();

        $order1 = Order::where('order_number', '001')->first();

        if (!$order1) {
            $order1 = Order::create([
                'order_number' => '001',
                'user_id' => $merchantOne->id
            ]);

            OrderProduct::create(
                [
                    'product_id' => $cookingOil->id,
                    'price' => 110,
                    'quantity' => 1,
                    'order_id' => $order1->id,
                ]
            );
            OrderProduct::create(

                [
                    'product_id' => $shoePolish->id,
                    'price' => 55,
                    'quantity' => 1,
                    'order_id' => $order1->id,
                ],
            );
        }

        $order2 = Order::where('order_number', '002')->first();

        if (!$order2) {
            $order2 = Order::create([
                'order_number' => '002',
                'user_id' => $merchantTwo->id
            ]);

            OrderProduct::create(
                [
                    'product_id' => $cookingOil->id,
                    'price' => 115,
                    'quantity' => 8,
                    'order_id' => $order2->id,
                ],
            );
            OrderProduct::create(
                [
                    'product_id' => $toothpaste->id,
                    'price' => 60,
                    'quantity' => 21,
                    'order_id' => $order2->id,
                ],
            );
        }
    }
}
