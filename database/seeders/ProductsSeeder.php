<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use App\Models\Master;
use App\Models\Company;
use App\Models\Product;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Hash;

class ProductsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cookingOil = Product::where('name', 'Cooking Oil')->first();
        if (!$cookingOil) {
            Product::create([
                'name' => 'Cooking Oil',
                'description' => '2kgs Cooking Oil',
                'model' => 'Kasuku',
            ]);
        }

        $shoePolish = Product::where('name', 'Shoe Polish')->first();
        if (!$shoePolish) {
            Product::create([
                'name' => 'Shoe Polish',
                'description' => '500gms Shoe Polish',
                'model' => 'Kiwi',
            ]);
        }

        $toothpaste = Product::where('name', 'Toothpaste')->first();
        if (!$toothpaste) {
            Product::create([
                'name' => 'Toothpaste',
                'description' => '100gms Toothpaste',
                'model' => 'Colgate',
            ]);
        }

        $maizeFlour = Product::where('name', 'Maize Flour')->first();
        if (!$maizeFlour) {
            Product::create([
                'name' => 'Maize Flour',
                'description' => '2kgs Maize Flour',
                'model' => 'Jembe',
            ]);
        }

        $yorghut = Product::where('name', 'Yorghut')->first();
        if (!$yorghut) {
            Product::create([
                'name' => 'Yorghut',
                'description' => '500ml Yorghut',
                'model' => 'Daima',
            ]);
        }
    }
}
