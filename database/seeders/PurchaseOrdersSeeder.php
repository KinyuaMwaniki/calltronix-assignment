<?php

namespace Database\Seeders;

use App\Models\Product;
use App\Models\Supplier;
use App\Models\OrderProduct;
use App\Models\PurchaseOrder;
use Illuminate\Database\Seeder;
use App\Models\PurchaseOrderProduct;

class PurchaseOrdersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cookingOil = Product::where('name', 'Cooking Oil')->first();
        $shoePolish = Product::where('name', 'Shoe Polish')->first();
        $toothpaste = Product::where('name', 'Toothpaste')->first();

        $order1 = PurchaseOrder::where('order_number', '001')->first();

        if (!$order1) {
            $order1 = PurchaseOrder::create([
                'order_number' => '001',
                'supplier_id' => 1
            ]);

            PurchaseOrderProduct::create(
                [
                    'product_id' => $cookingOil->id,
                    'price' => 100,
                    'quantity' => 2,
                    'order_id' => $order1->id,
                ]
            );
            PurchaseOrderProduct::create(

                [
                    'product_id' => $shoePolish->id,
                    'price' => 50,
                    'quantity' => 3,
                    'order_id' => $order1->id,
                ],
            );
        }

        $order2 = PurchaseOrder::where('order_number', '002')->first();

        if (!$order2) {
            $order2 = PurchaseOrder::create([
                'order_number' => '002',
                'supplier_id' => 2
            ]);

            PurchaseOrderProduct::create(
                [
                    'product_id' => $cookingOil->id,
                    'price' => 110,
                    'quantity' => 5,
                    'order_id' => $order2->id,
                ],
            );
            PurchaseOrderProduct::create(
                [
                    'product_id' => $toothpaste->id,
                    'price' => 40,
                    'quantity' => 10,
                    'order_id' => $order2->id,
                ],
            );
        }
    }
}
