<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use App\Models\Master;
use App\Models\Company;
use App\Models\Supplier;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Hash;

class SuppliersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $supplier1 = Supplier::where('email', 'jackdoe@mailinator.com')->first();

        if (!$supplier1) {
            $supplier1 = Supplier::create([
                'name' => 'Jack Doe',
                'email' => 'jackdoe@mailinator.com',
                'address' => 'Nairobi',
                'mobile_phone' => 071122334455
            ]);
        } else {
            $this->command->info("jackdoe@mailinator.com already exists");
        }

        $supplier2 = Supplier::where('email', 'anndoe@mailinator.com')->first();

        if (!$supplier2) {
            $supplier2 = Supplier::create([
                'name' => 'Anne Doe',
                'email' => 'anndoe@mailinator.com',
                'address' => 'Nanyuki',
                'mobile_phone' => 071122334456
            ]);
        } else {
            $this->command->info("anndoe@mailinator.com already exists");
        }

        $supplier3 = Supplier::where('email', 'jimdoe@mailinator.com')->first();

        if (!$supplier3) {
            $supplier2 = Supplier::create([
                'name' => 'Jim Doe',
                'email' => 'jimdoe@mailinator.com',
                'address' => 'Kisumu',
                'mobile_phone' => 071122334457
            ]);
        } else {
            $this->command->info("jimdoe@mailinator.com already exists");
        }
    }
}
