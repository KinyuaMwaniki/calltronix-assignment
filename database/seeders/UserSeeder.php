<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use App\Models\Master;
use App\Models\Company;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::where('email', 'janedoe@mailinator.com')->first();
        $password = 'password';

        if (!$user) {
            $admin_one = User::create([
                'name' => 'Jane Doe',
                'email' => 'janedoe@mailinator.com',
                'password' => Hash::make($password),
                'email_verified_at' => now(),
                'role' => 'Administrator'
            ]);
        } else {
            $this->command->info("janedoe@mailinator.com already exists, password is $password");
        }
    }
}
