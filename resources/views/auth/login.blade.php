<x-layouts.guest>
    <div class="container">
        <form method="POST" action="{{ route('login') }}">
            @csrf
            <div class="row justify-content-md-center">
                <div class="col-xl-4 col-lg-5 col-md-6 col-sm-12">
                    <div class="login-screen">
                        <div class="login-box">
                            <a href="#" class="login-logo">
                                <img src="/topbar/img/logo-dark.png" alt="Wafi Admin Dashboard" />
                            </a>
                            <h5>Welcome back,<br />Please Login to your Account.</h5>

                            <p>Jane Creds: janedoe@mailinator.com (password)</p>
                            <p>Merchant Creds: merchantone@mailinator.com (password)</p>
                            <div class="form-group">
                                <input name="email" type="email"
                                    class="form-control @error('email') is-invalid @enderror"
                                    placeholder="Email Address" required value="{{ old('email') }}"
                                    autocomplete="email" autofocus />
                                @error('email')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <input type="password" placeholder="Password" id="password"
                                    class="form-control @error('password') is-invalid @enderror" name="password"
                                    required autocomplete="current-password" />
                                @error('password')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="actions mb-4">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="remember" id="remember"
                                        {{ old('remember') ? 'checked' : '' }}>
                                    <label class="custom-control-label" for="remember">Remember me</label>
                                </div>
                                <button type="submit" class="btn btn-primary">Login</button>
                            </div>
                            @if (Route::has('password.request'))
                                <div class="forgot-pwd">
                                    <a class="link" href="{{ route('password.request') }}">Forgot password?</a>
                                </div>
                            @endif
                            <hr>
                            <div class="actions align-left">
                                <span class="additional-link">New here?</span>
                                <a href="{{ route('register') }}" class="btn btn-dark">Create an Account</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</x-layouts.guest>
