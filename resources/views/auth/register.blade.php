<x-layouts.guest>
    <div class="container">
        <form method="POST" action="{{ route('register') }}">
            @csrf
            <div class="row justify-content-md-center">
                <div class="col-xl-4 col-lg-5 col-md-6 col-sm-12">
                    <div class="login-screen">
                        <div class="login-box">
                            <a href="#" class="login-logo">
                                <img src="/topbar/img/logo-dark.png" alt="Wafi Admin Dashboard" />
                            </a>
                            <h5>Welcome,<br />Create your Admin Account.</h5>
                            <div class="form-group">
                                <input 
                                id="name" type="text"
                                    class="form-control @error('name') is-invalid @enderror" name="name"
                                    value="{{ old('name') }}" required autocomplete="name" autofocus
                                    placeholder="Name" 
                                    
                                    />
                                @error('name')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <input
                                
                                name="email" type="email"
                                    class="form-control @error('email') is-invalid @enderror"
                                    placeholder="Email Address" required value="{{ old('email') }}"
                                    autocomplete="email" autofocus
                                    
                                    />
                                @error('email')
                                    <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <input type="password" class="form-control @error('password') is-invalid @enderror"
                                        placeholder="Password" id="password" name="password" required
                                        autocomplete="new-password" />
                                    <input type="password" id="password-confirm" class="form-control"
                                        name="password_confirmation" required autocomplete="new-password"
                                        placeholder="Confirm Password" />
                                </div>
                                <small id="passwordHelpInline" class="text-muted">
                                    Password must be 8-20 characters long.
                                </small>
                                @error('password')
                                    <div class="invalid-feedback register">{{ $message }}</div>
                                @enderror
                            </div>


                            <div class="actions mb-4">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" name="remember" id="remember"
                                        {{ old('remember') ? 'checked' : '' }}>
                                    <label class="custom-control-label" for="remember">Remember me</label>
                                </div>
                                <button type="submit" class="btn btn-primary">Signup</button>
                            </div>
                            <hr>
                            <div class="m-0">
                                <span class="additional-link">Have an account? <a href="{{ route('login') }}"
                                        class="btn btn-dark">Login</a></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</x-layouts.guest>
