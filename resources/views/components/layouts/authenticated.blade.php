<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta name="description" content="Responsive Bootstrap4 Dashboard Template">
    <meta name="author" content="ParkerThemes">
    <link rel="shortcut icon" href="topbar/img/fav.png" />

    <title>{{ $title }}</title>

    <link rel="stylesheet" href="/topbar/css/bootstrap.min.css">
    <link rel="stylesheet" href="/topbar/fonts/style.css">
    <link rel="stylesheet" href="/topbar/css/main.css">
    <link rel="stylesheet" href="{{ asset('topbar/css/jithvar.css') }}">
    @stack('css')
</head>

<body>

    <div id="loading-wrapper">
        <div class="spinner-border" role="status">
            <span class="sr-only">Loading...</span>
        </div>
    </div>
    <header class="header">
        <div class="logo-wrapper">
            <a href="{{ route('home') }}" class="logo">
                <img src="/topbar/img/logo.png" alt="Wafi Admin Dashboard" />
            </a>
            <a href="#" class="quick-links-btn" data-toggle="tooltip" data-placement="right" title=""
                data-original-title="Quick Links">
                <i class="icon-menu1"></i>
            </a>
        </div>
        <div class="header-items">
            <!-- Custom search start -->
            <div class="custom-search">
                <input type="text" class="search-query" placeholder="Search here ...">
                <i class="icon-search1"></i>
            </div>
            <!-- Custom search end -->

            <!-- Header actions start -->
            <ul class="header-actions">
                <li class="dropdown">
                    <a href="#" id="notifications" data-toggle="dropdown" aria-haspopup="true">
                        <i class="icon-box"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right lrg" aria-labelledby="notifications">
                        <div class="dropdown-menu-header">
                            Tasks (05)
                        </div>
                        <ul class="header-tasks">
                            <li>
                                <p>#48 - Dashboard UI<span>90%</span></p>
                                <div class="progress">
                                    <div class="progress-bar bg-primary" role="progressbar" aria-valuenow="90"
                                        aria-valuemin="0" aria-valuemax="100" style="width: 90%">
                                        <span class="sr-only">90% Complete (success)</span>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <p>#95 - Alignment Fix<span>60%</span></p>
                                <div class="progress">
                                    <div class="progress-bar bg-primary" role="progressbar" aria-valuenow="60"
                                        aria-valuemin="0" aria-valuemax="100" style="width: 60%">
                                        <span class="sr-only">60% Complete (success)</span>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <p>#7 - Broken Button<span>40%</span></p>
                                <div class="progress">
                                    <div class="progress-bar bg-secondary" role="progressbar" aria-valuenow="40"
                                        aria-valuemin="0" aria-valuemax="100" style="width: 40%">
                                        <span class="sr-only">40% Complete (success)</span>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="dropdown">
                    <a href="#" id="notifications" data-toggle="dropdown" aria-haspopup="true">
                        <i class="icon-bell"></i>
                        <span class="count-label">8</span>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right lrg" aria-labelledby="notifications">
                        <div class="dropdown-menu-header">
                            Notifications (40)
                        </div>
                        <ul class="header-notifications">
                            <li>
                                <a href="#">
                                    <div class="user-img away">
                                        <img src="/topbar/img/user21.png" alt="User" />
                                    </div>
                                    <div class="details">
                                        <div class="user-title">Abbott</div>
                                        <div class="noti-details">Membership has been ended.</div>
                                        <div class="noti-date">Oct 20, 07:30 pm</div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div class="user-img busy">
                                        <img src="/topbar/img/user10.png" alt="User" />
                                    </div>
                                    <div class="details">
                                        <div class="user-title">Braxten</div>
                                        <div class="noti-details">Approved new design.</div>
                                        <div class="noti-date">Oct 10, 12:00 am</div>
                                    </div>
                                </a>
                            </li>
                            <li>
                                <a href="#">
                                    <div class="user-img online">
                                        <img src="/topbar/img/user6.png" alt="User" />
                                    </div>
                                    <div class="details">
                                        <div class="user-title">Larkyn</div>
                                        <div class="noti-details">Check out every table in detail.</div>
                                        <div class="noti-date">Oct 15, 04:00 pm</div>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                @auth
                    <li class="dropdown">
                        <a href="#" id="userSettings" class="user-settings" data-toggle="dropdown"
                            aria-haspopup="true">
                            <span class="user-name">{{ Auth::user()->name }}</span>
                            <span class="avatar">ZF<span class="status busy"></span></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userSettings">
                            <div class="header-profile-actions">
                                <div class="header-user-profile">
                                    <div class="header-user">
                                        <img src="/topbar/img/user.png" alt="Admin Template" />
                                    </div>
                                    <h5>{{ Auth::user()->name }}</h5>
                                    <p>Admin</p>
                                </div>
                                <a href="#"><i class="icon-user1"></i> My Profile</a>
                                <a href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                              document.getElementById('logout-form').submit();">
                                    <i class="icon-log-out1"></i> Sign Out
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                    @csrf
                                </form>
                            </div>
                        </div>
                    </li>
                @endauth
                <li>
                    <a href="#" class="quick-settings-btn" data-toggle="tooltip" data-placement="left"
                        title="" data-original-title="Quick Settings">
                        <i class="icon-list"></i>
                    </a>
                </li>
            </ul>
            <!-- Header actions end -->
        </div>
    </header>

    <div class="screen-overlay"></div>
    <div class="quick-links-box">
        <div class="quick-links-header">
            Quick Links
            <a href="#" class="quick-links-box-close">
                <i class="icon-circle-with-cross"></i>
            </a>
        </div>

        <div class="quick-links-wrapper">
            <div class="fullHeight">
                <div class="quick-links-body">
                    <div class="container-fluid p-0">
                        <div class="row less-gutters">
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                <a href="documents.html" class="quick-tile blue">
                                    <i class="icon-file-text"></i>
                                    Documents
                                </a>
                            </div>
                            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-12">
                                <a href="crm-dashboard.html" class="quick-tile secondary">
                                    <i class="icon-pie-chart1"></i>
                                    CRM
                                </a>
                            </div>
                            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-12">
                                <a href="ecommerce-dashboard.html" class="quick-tile blue">
                                    <i class="icon-shopping-cart1"></i>
                                    Ecommerce
                                </a>
                            </div>
                        </div>
                        <div class="row less-gutters">
                            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
                                <a href="gallery2.html" class="quick-tile photos lg">
                                    Photos
                                </a>
                            </div>
                            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-12">
                                <a href="tasks.html" class="quick-tile">
                                    <i class="icon-check-circle"></i>
                                    Tasks
                                </a>
                                <a href="calendar-external-draggable.html" class="quick-tile blue">
                                    <i class="icon-calendar1"></i>
                                    Events
                                </a>
                            </div>
                            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-12">
                                <a href="chat.html" class="quick-tile blue">
                                    <i class="icon-message-circle"></i>
                                    Chat
                                </a>
                                <a href="default-layout.html" class="quick-tile">
                                    <i class="icon-grid"></i>
                                    Layout
                                </a>
                            </div>
                        </div>
                        <div class="row less-gutters">
                            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-12">
                                <a href="custom-alerts.html" class="quick-tile secondary">
                                    <i class="icon-alert-triangle"></i>
                                    Alerts
                                </a>
                            </div>
                            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-12">
                                <a href="google-maps.html" class="quick-tile blue">
                                    <i class="icon-map-pin"></i>
                                    Maps
                                </a>
                            </div>
                            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-12">
                                <a href="https://www.gmail.com" target="_blank" class="quick-tile white">
                                    <i class="icon-drafts"></i>
                                    Gmail
                                </a>
                            </div>
                            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-12">
                                <a href="https://www.youtube.com" target="_blank" class="quick-tile secondary">
                                    <i class="icon-youtube"></i>
                                    Youtube
                                </a>
                            </div>
                        </div>
                        <div class="row less-gutters">
                            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-12">
                                <a href="user-profile.html" class="quick-tile">
                                    <i class="icon-account_circle"></i>
                                    Profile
                                </a>
                            </div>
                            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-12">
                                <a href="contacts.html" class="quick-tile">
                                    <i class="icon-phone"></i>
                                    Contacts
                                </a>
                            </div>
                            <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-12">
                                <a href="account-settings.html" class="quick-tile">
                                    <i class="icon-settings1"></i>
                                    Settings
                                </a>
                            </div>
                            @auth
                                <div class="col-xl-3 col-lg-3 col-md-3 col-sm-3 col-12">
                                    <a href="{{ route('logout') }}" class="quick-tile"
                                        onclick="event.preventDefault();
                                              document.getElementById('logout-form').submit();">
                                        <i class="icon-lock2"></i>
                                        Logout
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            @endauth
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="quick-settings-box">
        <div class="quick-settings-header">
            <div class="date-container">Today <span class="date" id="today-date"></span></div>
            <a href="#" class="quick-settings-box-close">
                <i class="icon-circle-with-cross"></i>
            </a>
        </div>
        <div class="quick-settings-body">
            <div class="fullHeight">
                <div class="quick-setting-list">
                    <h6 class="title">Events</h6>
                    <ul class="list-items">
                        <li>
                            <div class="list-title">Product Launch</div>
                            <div class="list-location">10:00 AM</div>
                        </li>
                        <li>
                            <div class="list-title">Team Meeting</div>
                            <div class="list-location">01:30 PM</div>
                        </li>
                        <li>
                            <div class="list-title">Q&A Session</div>
                            <div class="list-location">02:30 PM</div>
                        </li>
                    </ul>
                </div>
                <div class="quick-setting-list">
                    <h6 class="title">Notes</h6>
                    <ul class="list-items">
                        <li>
                            <div class="list-title">Refreshing the list</div>
                            <div class="list-location">03:15 PM</div>
                        </li>
                        <li>
                            <div class="list-title">Not able to click on button</div>
                            <div class="list-location">03:18 PM</div>
                        </li>
                    </ul>
                </div>
                <div class="quick-setting-list">
                    <h6 class="title">Quick Settings</h6>
                    <ul class="set-priority-list">
                        <li>
                            <div class="custom-control custom-switch">
                                <input type="checkbox" class="custom-control-input" checked id="systemUpdates">
                                <label class="custom-control-label" for="systemUpdates">System Updates</label>
                            </div>
                        </li>
                        <li>
                            <div class="custom-control custom-switch">
                                <input type="checkbox" class="custom-control-input" id="noti">
                                <label class="custom-control-label" for="noti">Notifications</label>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <nav class="navbar navbar-expand-lg custom-navbar">
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#WafiAdminNavbar"
                aria-controls="WafiAdminNavbar" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon">
                    <i></i>
                    <i></i>
                    <i></i>
                </span>
            </button>
            <div class="collapse navbar-collapse" id="WafiAdminNavbar">
                <ul class="navbar-nav">
                    <li class="nav-item dropdown">
                        <a @class([
                            'nav-link',
                            'dropdown-toggle',
                            'active-page' => $activePage === 'dashboard',
                        ]) href="#" id="dashboardsDropdown" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="icon-devices_other nav-icon"></i>
                            Dashboard
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="dashboardsDropdown">
                            <li>
                                <a class="dropdown-item active-page" href="{{ route('home') }}">Admin Dashboard</a>
                            </li>
                            <li>
                                <a class="dropdown-item" href="project-dashboard.html">Project Management</a>
                            </li>
                            <li>
                                <a class="dropdown-item" href="sales-dashboard.html">Sales Dashboard</a>
                            </li>
                            <li>
                                <a class="dropdown-item" href="crm-dashboard.html">CRM Dashboard</a>
                            </li>
                            <li>
                                <a class="dropdown-item" href="ecommerce-dashboard.html">Ecommerce Dashboard</a>
                            </li>
                            <li>
                                <a class="dropdown-item" href="quick-dashboard.html">Quick Dashboard</a>
                            </li>
                            <li>
                                <a class="dropdown-item" href="smart-dashboard.html">Smart Dashboard</a>
                            </li>
                            <li>
                                <a class="dropdown-item" href="helpdesk-dashboard.html">Help Desk Dashboard</a>
                            </li>
                            <li>
                                <a class="dropdown-toggle sub-nav-link" href="#" id="layoutsDropdown"
                                    role="button" data-toggle="dropdown" aria-haspopup="true"
                                    aria-expanded="false">
                                    Layouts
                                </a>
                                <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="layoutsDropdown">
                                    <li>
                                        <a class="dropdown-item" href="default-layout.html">Default Layout</a>
                                    </li>
                                    <li>
                                        <a class="dropdown-item" href="default-layout-light.html">Light Color
                                            Layout</a>
                                    </li>
                                    <li>
                                        <a class="dropdown-item" href="fixed-layout.html">Fixed Layout</a>
                                    </li>
                                    <li>
                                        <a class="dropdown-item" href="boxed-layout.html">Boxed Layout</a>
                                    </li>
                                    <li>
                                        <a class="dropdown-item" href="card-options.html">Card Options</a>
                                    </li>
                                    <li>
                                        <a class="dropdown-item" href="drag-drop-cards.html">Drag and Drop Cards</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    @if (Auth::user()->role === 'Administrator')
                        <li class="nav-item dropdown">
                            <a @class([
                                'nav-link',
                                'dropdown-toggle',
                                'active-page' => $activePage === 'suppliers',
                            ]) href="#" id="appsDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="icon-package nav-icon"></i>
                                Suppliers
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="appsDropdown">
                                <li>
                                    <a class="dropdown-item" href="{{ route('suppliers.index') }}">List Suppliers</a>
                                </li>
                                <li>
                                    <a class="dropdown-item" href="{{ route('suppliers.create') }}">Create
                                        Suppliers</a>
                                </li>
                            </ul>
                        </li>
                    @endif
                    <li class="nav-item dropdown">
                        <a @class([
                            'nav-link',
                            'dropdown-toggle',
                            'active-page' => $activePage === 'users',
                        ]) href="#" id="appsDropdown" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="icon-package nav-icon"></i>
                            Users
                        </a>
                        <ul class="dropdown-menu" aria-labelledby="appsDropdown">
                            <li>
                                <a class="dropdown-item" href="{{ route('users.index') }}">List Users</a>
                            </li>
                            <li>
                                <a class="dropdown-item" href="{{ route('users.create') }}">Create Users</a>
                            </li>
                        </ul>
                    </li>

                </ul>
            </div>
        </nav>
        {{ $slot }}
        <footer class="main-footer">© Wafi 2020</footer>
    </div>
    <script src="/topbar/js/jquery.min.js"></script>
    <script src="/topbar/js/bootstrap.bundle.min.js"></script>
    <script src="/topbar/js/moment.js"></script>
    <script src="/topbar/vendor/slimscroll/slimscroll.min.js"></script>
    <script src="/topbar/vendor/slimscroll/custom-scrollbar.js"></script>
    <script src="/topbar/js/main.js"></script>
    @stack('scripts')
    {{-- <script>
        window.onload = function() {
            console.log('onload');
            document.getElementById("loading-wrapper").style.display = "none"
        }
        // document.addEventListener("DOMContentLoaded", function(event) {
        //     document.getElementById("loading-wrapper").style.display = "none"
        //     console.log('DOMContentLoaded');
        // });
    </script> --}}
</body>

</html>
