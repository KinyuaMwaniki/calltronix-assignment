<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Responsive Bootstrap4 Dashboard Template">
    <meta name="author" content="ParkerThemes">
    <link rel="shortcut icon" href="topbar/img/fav.png" />
    <title>Wafi Admin Template - Forgot Password</title>
    <link rel="stylesheet" href="/topbar/css/bootstrap.min.css" />
    <link rel="stylesheet" href="/topbar/css/main.css" />
    <link rel="stylesheet" href="{{ asset('topbar/css/jithvar.css') }}">
</head>

<body class="authentication">
    {{ $slot }}
</body>

</html>
