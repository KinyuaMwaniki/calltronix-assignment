<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
    <div class="form-group">
        <label for="name" class="col-form-label">Name</label>
        <input tabindex="1" id="name" type="text" class="form-control @error('name') is-invalid @enderror"
            name="name" value="{{ old('name', isset($supplier) ? $supplier->name : '') }}" required autocomplete="name"
            autofocus placeholder="Name" />
        @error('name')
            <div class="invalid-feedback">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
        <label for="mobile_phone" class="col-form-label">Mobile Phone</label>
        <input tabindex="3" id="mobile_phone" type="text"
            class="form-control @error('mobile_phone') is-invalid @enderror" name="mobile_phone"
            value="{{ old('mobile_phone', isset($supplier) ? $supplier->mobile_phone : '') }}" required
            placeholder="Mobile Phone" />
        @error('phone')
            <div class="invalid-feedback">{{ $message }}</div>
        @enderror
    </div>
</div>
<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
    <div class="form-group">
        <label for="inputEmail" class="col-form-label">E-mail</label>
        <input tabindex="2" id="email" name="email" type="email"
            class="form-control @error('email') is-invalid @enderror" placeholder="Email Address" required
            value="{{ old('email', isset($supplier) ? $supplier->email : '') }}" autocomplete="email" autofocus />
        @error('email')
            <div class="invalid-feedback">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
        <label for="address" class="col-form-label">Address</label>
        <input tabindex="4" id="address" type="text" class="form-control @error('address') is-invalid @enderror"
            name="address" value="{{ old('address', isset($supplier) ? $supplier->address : '') }}" required
            autocomplete="name" autofocus placeholder="Address" />
        @error('address')
            <div class="invalid-feedback">{{ $message }}</div>
        @enderror
    </div>
</div>
