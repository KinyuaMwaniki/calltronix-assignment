<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
    <div class="form-group">
        <label for="name" class="col-form-label">Name</label>
        <input tabindex="1" id="name" type="text" class="form-control @error('name') is-invalid @enderror"
            name="name" value="{{ old('name', $user->name) }}" required autocomplete="name" autofocus
            placeholder="Name" />
        @error('name')
            <div class="invalid-feedback">{{ $message }}</div>
        @enderror
    </div>
    @if (empty($user->id))
        <div class="form-group">
            <label for="password" class="col-form-label">Password</label>
            <input tabindex="3" id="password" type="password"
                class="form-control @error('password') is-invalid @enderror" name="password"
                value="{{ old('password') }}" required autocomplete="new-password" placeholder="Password" />
            @error('password')
                <div class="invalid-feedback">{{ $message }}</div>
            @enderror
        </div>
    @endif
    <div class="form-group">
        <label for="country_code" class="col-form-label">Country Code</label>
        <input tabindex="5" id="country_code" type="text"
            class="form-control @error('country_code') is-invalid @enderror" name="country_code"
            value="{{ old('country_code', $user->country_code) }}" required placeholder="Country Code" />
        @error('country_code')
            <div class="invalid-feedback">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
        <label for="company_name" class="col-form-label">Company Name</label>
        <input tabindex="7" id="company_name" type="text"
            class="form-control @error('company_name') is-invalid @enderror" name="company_name"
            value="{{ old('company_name', $user->company_name) }}" required placeholder="Company Name" />
        @error('company_name')
            <div class="invalid-feedback">{{ $message }}</div>
        @enderror
    </div>
</div>
<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
    <div class="form-group">
        <label for="inputEmail" class="col-form-label">E-mail</label>
        <input tabindex="2" id="email" name="email" type="email"
            class="form-control @error('email') is-invalid @enderror" placeholder="Email Address" required
            value="{{ old('email', $user->email) }}" autocomplete="email" autofocus />
        @error('email')
            <div class="invalid-feedback">{{ $message }}</div>
        @enderror
    </div>
    @if (empty($user->id))
        <div class="form-group">
            <label for="password_confirmation" class="col-form-label">Password
                Confirmation</label>
            <input tabindex="4" id="password_confirmation" type="password" class="form-control"
                name="password_confirmation" required autocomplete="new-password" placeholder="Password Confirmation" />
        </div>
    @endif
    <div class="form-group">
        <label for="phone" class="col-form-label">Phone</label>
        <input tabindex="6" id="phone" type="text" class="form-control @error('phone') is-invalid @enderror"
            name="phone" value="{{ old('phone', $user->phone) }}" required placeholder="Phone" />
        @error('phone')
            <div class="invalid-feedback">{{ $message }}</div>
        @enderror
    </div>
    <div class="form-group">
        <label for="active" class="col-form-label">Active</label>
        <select tabindex="9" id="active" class="form-control" name="active">
            <option value="0" {{ $user->active == 0 ? 'selected' : '' }}>No</option>
            <option value="1" {{ $user->active == 1 ? 'selected' : '' }}>Yes</option>
        </select>
    </div>
</div>
