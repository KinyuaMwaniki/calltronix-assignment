<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Responsive Bootstrap4 Dashboard Template">
    <meta name="author" content="ParkerThemes">
    <link rel="shortcut icon" href="topbar/img/fav.png" />
    <title>404</title>
    <link href="https://fonts.googleapis.com/css?family=Bebas+Neue|Girassol|ZCOOL+KuaiLe&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/topbar/css/bootstrap.min.css">
    <link rel="stylesheet" href="/topbar/fonts/style.css">
    <link rel="stylesheet" href="/topbar/css/main.css">
    <link rel="stylesheet" href="/topbar/vendor/particles/particles.css">
</head>

<body class="authentication">
    <div id="particles-js"></div>
    <div class="countdown-bg"></div>
    <div class="error-screen">
        <h1>404</h1>
        <h5>We're sorry but it looks<br />like that page doesn't exist anymore.</h5>
        <a href="{{ route('home') }}" class="btn btn-secondary">Go back to Dashboard</a>
    </div>
    <script src="/topbar/js/jquery.min.js"></script>
    <script src="/topbar/js/bootstrap.bundle.min.js"></script>
    <script src="/topbar/js/moment.js"></script>
    <script src="/topbar/vendor/particles/particles.min.js"></script>
    <script src="/topbar/vendor/particles/particles-custom-error.js"></script>
</body>

</html>
