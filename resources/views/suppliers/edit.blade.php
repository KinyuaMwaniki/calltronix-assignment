<x-layouts.authenticated title="Suppliers" active-page="suppliers">
    <div class="main-container">
        <div class="page-header">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">Suppliers</li>
                <li class="breadcrumb-item active">Edit</li>
            </ol>
            <ul class="app-actions">
                <li>
                    <a href="#" id="reportrange">
                        <span class="range-text">Sep 8, 2022 - Oct 7, 2022</span>
                        <i class="icon-chevron-down"></i>
                    </a>
                </li>
                <li>
                    <a href="#" data-toggle="tooltip" data-placement="top" title=""
                        data-original-title="Print">
                        <i class="icon-print"></i>
                    </a>
                </li>
                <li>
                    <a href="#" data-toggle="tooltip" data-placement="top" title=""
                        data-original-title="Download CSV">
                        <i class="icon-cloud_download"></i>
                    </a>
                </li>
            </ul>
        </div>
        <div class="content-wrapper">
            <div class="row justify-content-center gutters">
                <div class="col-xl-10 col-lg-10 col-md-12 col-sm-12 col-12">
                    <form id="supplier-form" method="POST" action="{{ route('suppliers.update', $supplier->id) }}"
                        novalidate>
                        @method('PUT')
                        @csrf
                        <div class="card m-0">
                            <div class="card-header">
                                <div class="card-title">New Supplier</div>
                            </div>
                            <div class="card-body">
                                <div class="row gutters">
                                    <x-suppliers.fields :supplier="$supplier" />
                                </div>
                                <div class="row gutters">
                                    <div class="col-xl-12">
                                        <button id="submit" type="submit" class="btn btn-primary float-right">
                                            Update
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @push('scripts')
        <script>
            const supplierForm = document.getElementById('supplier-form');
            const name = document.getElementById('name');
            const email = document.getElementById('email');
            const mobile_phone = document.getElementById('mobile_phone');

            supplierForm.addEventListener('submit', (event) => {
                if (validate()) {
                    supplierForm.submit();
                    return;
                }
                event.preventDefault();
            });

            function clearClasses() {
                name.classList.remove("is-invalid");
                email.classList.remove("is-invalid");
                mobile_phone.classList.remove("is-invalid");
            }

            function validate() {
                clearClasses();
                let isValid = true;
                if (name.value === '') {
                    name.classList.add("is-invalid")
                    isValid = false;
                }
                if (email.value === '') {
                    email.classList.add("is-invalid")
                    isValid = false;
                }
                if (mobile_phone.value === '') {
                    mobile_phone.classList.add("is-invalid")
                    isValid = false;
                }
                return isValid;
            }
        </script>
    @endpush
</x-layouts.authenticated>
