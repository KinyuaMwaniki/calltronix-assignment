<x-layouts.authenticated title="Suppliers" active-page="suppliers">
    @push('css')
        <link rel="stylesheet" href="/topbar/vendor/datatables/dataTables.bs4.css" />
        <link rel="stylesheet" href="/topbar/vendor/datatables/dataTables.bs4-custom.css" />
    @endpush

    <div class="main-container">
        <div class="page-header">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">Tables</li>
                <li class="breadcrumb-item active">Data Tables</li>
            </ol>

            <ul class="app-actions">
                <li>
                    <a href="#" id="reportrange">
                        <span class="range-text">Sep 7, 2022 - Oct 6, 2022</span>
                        <i class="icon-chevron-down"></i>
                    </a>
                </li>
                <li>
                    <a href="#" data-toggle="tooltip" data-placement="top" title=""
                        data-original-title="Print">
                        <i class="icon-print"></i>
                    </a>
                </li>
                <li>
                    <a href="#" data-toggle="tooltip" data-placement="top" title=""
                        data-original-title="Download CSV">
                        <i class="icon-cloud_download"></i>
                    </a>
                </li>
            </ul>
        </div>
        <div class="content-wrapper">
            <div class="row gutters">
                <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">

                    <div class="table-container" id="app">
                        <div class="t-header">
                            <p>Supplier</p>
                            <a href="{{ route('suppliers.create') }}" class="btn btn-primary btn-lg">
                                Add New
                                Supplier
                            </a>
                        </div>
                        <suppliers-index></suppliers-index>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @push('scripts')
        @vite('resources/js/app.js')
    @endpush
</x-layouts.authenticated>
