<x-layouts.authenticated title="Users" active-page="users">
    <div class="main-container">
        <div class="page-header">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">Users</li>
                <li class="breadcrumb-item active">Edit</li>
            </ol>
            <ul class="app-actions">
                <li>
                    <a href="#" id="reportrange">
                        <span class="range-text">Sep 8, 2022 - Oct 7, 2022</span>
                        <i class="icon-chevron-down"></i>
                    </a>
                </li>
                <li>
                    <a href="#" data-toggle="tooltip" data-placement="top" title=""
                        data-original-title="Print">
                        <i class="icon-print"></i>
                    </a>
                </li>
                <li>
                    <a href="#" data-toggle="tooltip" data-placement="top" title=""
                        data-original-title="Download CSV">
                        <i class="icon-cloud_download"></i>
                    </a>
                </li>
            </ul>
        </div>
        <div class="content-wrapper">
            <div class="row justify-content-center gutters">
                <div class="col-xl-10 col-lg-10 col-md-12 col-sm-12 col-12">
                    <form id="user-form" method="POST" action="{{ route('users.update', $user->id) }}" novalidate>
                        @method('PUT')
                        @csrf
                        <div class="card m-0">
                            <div class="card-header">
                                <div class="card-title">New User</div>
                            </div>
                            <div class="card-body">
                                <div class="row gutters">
                                    <x-users.fields :user="$user" />
                                </div>
                                <div class="row gutters">
                                    <div class="col-xl-12">
                                        <button id="submit" type="submit" class="btn btn-primary float-right">
                                            Update
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @push('scripts')
        <script>
            const userForm = document.getElementById('user-form');
            const name = document.getElementById('name');
            const email = document.getElementById('email');
            const country_code = document.getElementById('country_code');
            const phone = document.getElementById('phone');
            const company_name = document.getElementById('company_name');

            userForm.addEventListener('submit', (event) => {
                if (validate()) {
                    console.log("Form submitting");
                    userForm.submit();
                    return;
                }
                event.preventDefault();
            });

            function clearClasses() {
                name.classList.remove("is-invalid");
                email.classList.remove("is-invalid");
                country_code.classList.remove("is-invalid");
                phone.classList.remove("is-invalid");
                company_name.classList.remove("is-invalid");
            }

            function validate() {
                console.log('validate');
                clearClasses();
                let isValid = true;
                console.log('name.value');
                console.log(name.value);
                if (name.value === '') {
                    name.classList.add("is-invalid")
                    isValid = false;
                }
                if (email.value === '') {
                    email.classList.add("is-invalid")
                    isValid = false;
                }
                if (country_code.value === '') {
                    country_code.classList.add("is-invalid")
                    isValid = false;
                }
                if (phone.value === '') {
                    phone.classList.add("is-invalid")
                    isValid = false;
                }
                if (company_name.value === '') {
                    company_name.classList.add("is-invalid")
                    isValid = false;
                }
                return isValid;
            }
        </script>
    @endpush
</x-layouts.authenticated>
